import datetime
import logging
from contextlib import contextmanager
from enum import Enum
from functools import wraps

from sqlalchemy import Column, Integer, inspect, String, Float, Boolean, DateTime, Date, and_, or_
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import Query, Session
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm.collections import InstrumentedList
from sqlalchemy.pool import QueuePool

from pyboot2.decorator import Decorator
from pyboot2.exception import InvalidInputException
from pyboot2.model import Model
from pyboot2.page import Page


class DbConf(object):
    def __init__(self, host=None, port=3306, username=None, password=None, db_name=None, charset="utf8",
                 init_pool_size=1, max_pool_size=5, pool_recycle_delay=600, sql_logging=False):
        self.host = host
        self.port = port
        self.username = username
        self.password = password
        self.db_name = db_name
        self.charset = charset
        self.init_pool_size = init_pool_size
        self.max_pool_size = max_pool_size
        self.pool_recycle_delay = pool_recycle_delay
        self.sql_logging = sql_logging
        self.__Session = None

    def init(self):
        self.__Session = sessionmaker(autocommit=False, autoflush=False, expire_on_commit=False, bind=self.get_engine())
        logging.debug("DbConfig initialized")
        return self

    def get_engine(self):
        db_baseurl = "mysql://%s:%s@%s:%s/%s?charset=%s" % (
            self.username, self.password, self.host, self.port, self.db_name, self.charset)
        logging.info("DB Baseurl: %s, Init pool size: %s, Max pool size: %s, Pool recycle delay: %s" % (
            db_baseurl, self.init_pool_size, self.max_pool_size, self.pool_recycle_delay))
        return create_engine(db_baseurl, echo=self.sql_logging, poolclass=QueuePool, pool_size=self.init_pool_size,
                             max_overflow=int(self.max_pool_size) - int(self.init_pool_size),
                             pool_recycle=int(self.pool_recycle_delay))

    def __get_session(self) -> Session:
        return self.__Session()

    @contextmanager
    def get_session(self):
        session = self.__get_session()
        try:
            yield session
        except Exception:
            session.rollback()
            raise
        finally:
            session.close()


class FilterOperationEnum(Enum):
    IN = "in"
    EQUAL = "equal"
    NOT_EQUAL = "not_equal"
    RANGE = "range"
    LIKE = "like"


class FilterOperation:
    def __init__(self, operation: FilterOperationEnum, column: str, value):
        self.operation = operation.value
        self.column = column
        self.value = value

    def db_filter(self):
        return {
            "op": self.operation,
            "column": self.column,
            "value": self.value
        }


class DBFilters:
    def __init__(self):
        self.db_filters = []

    def not_equal(self, key: str, value):
        self.db_filters.append(FilterOperation(FilterOperationEnum.NOT_EQUAL, key, value).db_filter())
        return self

    def equal(self, key: str, value):
        self.db_filters.append(FilterOperation(FilterOperationEnum.EQUAL, key, value).db_filter())
        return self

    def in_(self, key: str, value: list):
        self.db_filters.append(FilterOperation(FilterOperationEnum.IN, key, value).db_filter())
        return self

    def raw(self):
        return self.db_filters

    def like(self, key, value):
        self.db_filters.append(FilterOperation(FilterOperationEnum.LIKE, key, value).db_filter())
        return self

        # def or_(self, key, value):
        #     self.db_filters.append(FilterOperation(FilterOperationEnum.OR, key, value).db_filter())
        #     return self
        #
        # def range(self, key, start_value, end_value):
        #     self.db_filters.append(FilterOperation(FilterOperationEnum.RANGE, key, start_value, end_value).db_filter())
        #     return self


class DatabaseModel(Model):
    id = Column(Integer, autoincrement=True, primary_key=True, nullable=False)  # type: int

    def __init__(self):
        self.__obj_always = False

    def to_dict(self):
        obj_dict = super().to_dict()
        if self.id: obj_dict["id"] = self.id
        return obj_dict

    def from_dict(self, obj_dict: dict):
        if not obj_dict: return
        if "id" in obj_dict: self.id = obj_dict["id"]
        super().from_dict(obj_dict)
        return self

    @classmethod
    def get(cls, session: Session, id: int, include: list = None):
        query = session.query(cls)  # type: Query
        query = cls.join_tables(query, include)
        return query.get(id)

    @classmethod
    def get_first(cls, session: Session, filters=None, start: int = None, order_by=None, include: list = None):
        db_filters = filters.raw() if isinstance(filters, DBFilters) else filters
        query = cls._get_all_query(session, filters=db_filters, include=include)
        query = cls.limit_order_by_query(query, start=start, count=1, order_by=order_by)
        return query.first()

    @classmethod
    def get_count(cls, session: Session, filters=None, start: int = None, order_by=None, include: list = None):
        db_filters = filters.raw() if isinstance(filters, DBFilters) else filters
        query = cls._get_all_query(session, filters=db_filters, include=include)
        query = cls.limit_order_by_query(query, start=start, order_by=order_by)
        return query.count()

    @classmethod
    def get_all(cls, session: Session, filters=None, start: int = None, count: int = None, order_by=None,
                include: list = None) -> list:
        """ Filter structure
        filters= [
            {
                "or": [
                    {op:"in", column:'column', value:'value'},
                    {op:"equal", column:'column', value:'value'},
                    {op:"range", column:'column', value:['start_value', 'end_value']},
                    {op:"ilike", column:'column', value:'value'}
                 ]
            },
            {op:"in", column:'column', value:'value'},
            {op:"equal", column:'column', value:'value'},
            {op:"not_equal", column:'column', value:'value'},
            {op:"range", column:'column', value:['start_value', 'end_value']},
            {op:"ilike", column:'column', value:'value'}
        ]

        Example of filter query:
        filter_query = []
        filter_query.append(FilterOperation(FilterOperationEnum.IN, "id", [1, 5,10,12]).db_filter())
        filter_query.append(FilterOperation(FilterOperationEnum.EQUAL, "name", "TestName").db_filter())

        # or query
        filter_or_query = []
        filter_or_query.append(FilterOperation(FilterOperationEnum.IN, "user_id", [1, 5,10,12]).db_filter())
        filter_or_query.append(FilterOperation(FilterOperationEnum.IN, "customer_id", [1, 5,10,12]).db_filter())
        filter_query.append({"or": filter_or_query})

        # Order by format
        order_by = [{"created_at: "desc"}]
        """

        db_filters = filters.raw() if isinstance(filters, DBFilters) else filters
        query = cls._get_all_query(session, filters=db_filters, include=include)
        query = cls.limit_order_by_query(query, start=start, count=count, order_by=order_by)
        return query.all()

    @classmethod
    def get_page(cls, session: Session, filters=None, start: int = 0, count: int = 25, order_by=None,
                 include: list = None) -> Page:
        db_filters = filters.raw() if isinstance(filters, DBFilters) else filters
        query = cls._get_all_query(session, filters=db_filters, include=include)

        page = Page()
        page.items = cls.limit_order_by_query(query, start=start, count=count + 1, order_by=order_by).all()
        page.total_count = query.count()
        page.gen_page_data(start, count)
        return page

    @classmethod
    def delete(cls, session: Session, id: int):
        session.query(cls).filter(cls.id == id).delete()

    @classmethod
    def join_tables(cls, query: Query, include: list = None) -> Query:
        return query

    @classmethod
    def limit_order_by_query(cls, query: Query, start: int = None, count: int = None, order_by=None) -> Query:
        if order_by is not None:
            order_by_query = []
            columns = inspect(cls).columns
            for order_by_element in order_by:
                for key in order_by_element:
                    if key not in columns: continue
                    if order_by_element[key] == "asc":
                        order_by_query.append(columns[key].asc())
                    else:
                        order_by_query.append(columns[key].desc())
            query = query.order_by(*order_by_query)
        if start: query = query.offset(start)
        if count: query = query.limit(count)
        return query

    @classmethod
    def _get_structure(cls):
        if cls._structure: return cls._structure

        columns = inspect(cls).columns
        if not columns: return None
        cls._structure = {}
        for column in columns:
            type = column.type
            if isinstance(type, String):
                cls._structure[column.key] = str
            elif isinstance(type, Integer):
                cls._structure[column.key] = int
            elif isinstance(type, Float):
                cls._structure[column.key] = float
            elif isinstance(type, Boolean):
                cls._structure[column.key] = bool
            elif isinstance(type, DateTime):
                cls._structure[column.key] = datetime.datetime
            elif isinstance(type, Date):
                cls._structure[column.key] = datetime.date
            else:
                cls._structure[column.key] = None

        relationships = inspect(cls).relationships
        for relation in relationships:
            if relation.uselist:
                cls._structure[relation.key] = InstrumentedList
            else:
                cls._structure[relation.key] = Model
        return cls._structure

    @classmethod
    def _get_all_query(cls, session: Session, filters: list = None, include: list = None):
        columns = inspect(cls).columns
        query = session.query(cls)
        query = cls.join_tables(query, include)
        query = cls._gen_filter_query(query, filters, columns)
        return query

    @classmethod
    def _gen_filter_query(cls, query, filters: list, columns):
        if not filters or not isinstance(filters, list): return query
        or_filters_list = []
        and_filters_list = []
        like_filters_list = []
        for and_filter in filters:
            if "or" in and_filter and and_filter["or"] and isinstance(and_filter["or"], list):
                for or_filter in and_filter["or"]:
                    or_operation = cls._gen_operation_query(or_filter, columns)
                    if or_operation is None: continue
                    if isinstance(or_operation, list):
                        or_filters_list.extend(or_operation)
                    else:
                        or_filters_list.append(or_operation)
            else:
                and_operation = cls._gen_operation_query(and_filter, columns)
                if and_operation is None: continue
                if isinstance(and_operation, list):
                    and_filters_list.extend(and_operation)
                else:
                    and_filters_list.append(and_operation)

        if and_filters_list and len(and_filters_list) > 0:
            query = query.filter(and_(*and_filters_list))
        if or_filters_list and len(or_filters_list) > 0:
            query = query.filter(or_(*or_filters_list))
        return query

    @classmethod
    def _gen_operation_query(cls, input_filter, columns):
        # filters_list = []
        if not input_filter: return
        operation = input_filter["op"] if "op" in input_filter else None
        column_name = input_filter["column"] if "column" in input_filter else None
        value = input_filter["value"] if "value" in input_filter else None

        if column_name not in columns: return
        if not operation or not column_name: return

        if operation == FilterOperationEnum.IN.value:
            return columns[column_name].in_(value)
        elif operation == FilterOperationEnum.EQUAL.value:
            return columns[column_name] == value
        elif operation == FilterOperationEnum.NOT_EQUAL.value:
            return columns[column_name] != value
        elif operation == FilterOperationEnum.LIKE.value:
            return columns[column_name].like("%" + value + "%")
        elif operation == FilterOperationEnum.RANGE.value:
            range_query = []
            if isinstance(value, list):
                if len(value) == 1 or len(value) == 2 and value[0]:
                    range_query.append(columns[column_name] > value[0])
                else:
                    range_query.append(columns[column_name] < value[1])
                return range_query

                # def to_dict_deep(self, obj_always: bool = False) -> dict:
                #     self.__obj_always = obj_always
                #     return super().to_dict_deep()
                #
                #     # if not obj_always:
                #     #     return super().to_dict_deep()
                #     #
                #     # obj_dict = self.to_dict()
                #     # structure = self.__class__._get_structure()
                #     # if not structure: return obj_dict
                #     # for field_name in structure.keys():
                #     #     obj_type = structure[field_name]
                #     #     if issubclass(obj_type, Model):
                #     #         self._include_obj(obj_dict, field_name)
                #     #     elif obj_type is list:
                #     #         self._include_obj_list(obj_dict, field_name)
                #     # return obj_dict
                #
                # def from_dict_deep(self, obj_dict: dict, obj_always: bool = False):
                #     self.__obj_always = obj_always
                #     return super().from_dict_deep(obj_dict)
                #
                #     # if not obj_always:
                #     #     return super().from_dict_deep(obj_dict)
                #     #
                #     # self.from_dict(obj_dict)
                #     # structure = self.__class__._get_structure()
                #     # if not structure: return self
                #     # for field_name in structure:
                #     #     obj_type = structure[field_name]
                #     #     if issubclass(obj_type, Model):
                #     #         self._exclude_obj(obj_dict, field_name)
                #     # return self

                # def _include_obj_list(self, obj_dict: dict, name: str):
                #     if not self.__obj_always:
                #         return super()._include_obj_list(obj_dict, name)
                #
                #     json_list = []
                #     obj_list = getattr(self, name, None)
                #     if obj_list and isinstance(obj_list, list):
                #         for obj in obj_list:
                #             if isinstance(obj, Model):
                #                 json_list.append(obj.to_dict_deep())
                #             else:
                #                 json_list.append(obj)
                #     obj_dict[name] = json_list

                # def _include_obj(self, obj_dict: dict, name: str):
                #     if not self.__obj_always:
                #         return super()._include_obj(obj_dict, name)
                #
                #     field_name = name + "_id"
                #
                #     sub_obj = getattr(self, name, None)
                #     value = getattr(self, field_name, None) if field_name in self.__dict__ else None
                #     if sub_obj:
                #         if isinstance(sub_obj, Model): obj_dict[name] = sub_obj.to_dict_deep()
                #     elif value:
                #         obj_dict[name] = {"id": value}
                #
                #     if field_name in obj_dict: del obj_dict[field_name]
                #
                # def _exclude_obj(self, obj_dict: dict, name: str):
                #     # if not self.obj_always:
                #     #     return super()._exclude_obj(obj_dict, name)
                #
                #     if not obj_dict or name not in obj_dict: return
                #     obj = obj_dict[name]
                #     field_name = name + "_id"
                #     if "id" in obj and field_name in self.__class__._structure: setattr(self, field_name, obj["id"])


# Derive this class and implement _get_stored_session() and _set_stored_session()
class DatabaseManager(Decorator):
    def __init__(self, name, db_conf):
        if not db_conf: raise InvalidInputException(f"Database conf can not be empty")

        self.__name = name
        self.__db = DbConf(host=db_conf["host"], port=db_conf["port"], username=db_conf["username"],
                           password=db_conf["password"],
                           db_name=db_conf["db_name"], charset=db_conf["charset"],
                           init_pool_size=db_conf["init_pool_size"],
                           max_pool_size=db_conf["max_pool_size"], pool_recycle_delay=db_conf["pool_recycle_delay"],
                           sql_logging=db_conf["sql_logging"]).init()

    def _get_stored_session(self, name):
        raise Exception("Method get_current_session() not implemented")

    def _set_stored_session(self, name, session):
        raise Exception("Method set_current_session() not implemented")

    def require_session(self):
        def decorator(f):
            @wraps(f)
            def require_session_handler(*args, **kwargs):
                if self.__db is None: raise Exception("Db object is not set")

                if self._get_stored_session(self.__name) is None:
                    with self.__db.get_session() as session:
                        self._set_stored_session(self.__name, session)
                        r = f(*args, **kwargs)
                        session.commit()
                        return r
                else:
                    return f(*args, **kwargs)

            return require_session_handler

        return decorator

    def get_session(self) -> Session:
        return self._get_stored_session(self.__name)

    def get_db(self):
        return self.__db


DBModelBase = declarative_base(cls=DatabaseModel)
