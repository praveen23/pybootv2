import logging
import logging.config

import yaml


class LoggingConf(object):
    def __init__(self, logger_conf_file: str = None):
        self.logger_conf_file = logger_conf_file

    def init(self):
        stream = open(self.logger_conf_file, "r")
        logging.config.dictConfig(yaml.load(stream))
        stream.close()
        logging.debug("Logging initialized")

    def get_logger_conf_file(self) -> str:
        return self.logger_conf_file

    def set_logger_conf_file(self, logger_conf_file: str = None):
        self.logger_conf_file = logger_conf_file
