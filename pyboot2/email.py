import datetime
import logging
import smtplib
from contextlib import contextmanager
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from smtpd import COMMASPACE
from smtplib import SMTP

from pyboot2.util import DateTimeUtil

BODY_TYPE_PLAIN = "plain"
BODY_TYPE_HTML = "html"


class EmailClient:
    def __init__(self, host, port, username, password, is_tls=False, enabled=True):
        self.__host = host  # type: str
        self.__port = int(port)  # type: int
        self.__username = username  # type: str
        self.__password = password  # type: str
        self.__is_tls = is_tls  # type: bool
        self.__connection = None  # type: SMTP
        self.__enabled = enabled

    def connect(self):
        if not self.__enabled:
            logging.info("EmailClient is disabled, not connecting to server")
            return

        self.__connection = smtplib.SMTP(self.__host, self.__port)
        if self.__is_tls:
            self.__connection.ehlo()
            self.__connection.starttls()
        self.__connection.login(self.__username, self.__password)

    def send_email(self, sender, to_addresses, subject, body, body_type=BODY_TYPE_PLAIN, reply_addresses=None,
                   cc_addresses=None, bcc_addresses=None, attachments=None):
        if not self.__enabled:
            logging.info("EmailClient is disabled, not sending email")
            return

        message = MIMEMultipart()
        message.set_charset('utf-8')

        message['Subject'] = subject
        message['From'] = sender
        message['To'] = self.__convert_to_strings(to_addresses)
        if cc_addresses: message['CC'] = self.__convert_to_strings(cc_addresses)
        if bcc_addresses: message['BCC'] = self.__convert_to_strings(bcc_addresses)

        if reply_addresses:
            message['Reply-To'] = self.__convert_to_strings(reply_addresses)

        message.attach(MIMEText(body, body_type, "utf-8"))
        if attachments and len(attachments) > 0:
            for attachment in attachments:
                if attachment["type"] == "file":
                    part = MIMEApplication(attachment["file"].read())
                elif attachment["type"] == "filename":
                    with open(attachment["filename"], 'rb') as f:
                        part = MIMEApplication(f.read())
                part.add_header('Content-Disposition', 'attachment', filename=attachment["name"])
                message.attach(part)

        receipients = []
        if to_addresses and isinstance(to_addresses, list):
            receipients += to_addresses
        elif isinstance(to_addresses, str):
            receipients.append(to_addresses)

        if cc_addresses and isinstance(cc_addresses, list):
            receipients += cc_addresses
        elif isinstance(cc_addresses, str):
            receipients.append(cc_addresses)

        if bcc_addresses and isinstance(bcc_addresses, list):
            receipients += bcc_addresses
        elif isinstance(bcc_addresses, str):
            receipients.append(bcc_addresses)

        logging.debug(message.as_string())
        return self.__connection.sendmail(sender, receipients, message.as_string())

    def __convert_to_strings(self, list_of_strs):
        if isinstance(list_of_strs, (list, tuple)):
            result = COMMASPACE.join(list_of_strs)
        else:
            result = list_of_strs
        return result

    def disconnect(self):
        if not self.__enabled: return
        if self.__connection is not None: self.__connection.close()


class EmailConf:
    def __init__(self, host=None, port=25, username=None, password=None, is_tls=False, enabled=True):
        self.host = host
        self.port = port
        self.username = username
        self.password = password
        self.is_tls = is_tls
        self.enabled = enabled

    @contextmanager
    def get_client(self):
        start_time = datetime.datetime.now()
        client = EmailClient(self.host, self.port, self.username, self.password, self.is_tls, self.enabled)
        client.connect()
        conn_time = datetime.datetime.now()
        logging.debug("Email client connection time: %s milliseconds" % DateTimeUtil.diff(start_time, conn_time))

        try:
            yield client  # type: EmailClient
        finally:
            client.disconnect()
            logging.debug("Email client transaction time: %s milliseconds"
                          % DateTimeUtil.diff(conn_time, datetime.datetime.now()))
