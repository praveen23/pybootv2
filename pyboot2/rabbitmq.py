import datetime
from contextlib import contextmanager

from pika import BlockingConnection, ConnectionParameters
from pika.channel import Channel
from pika.connection import Connection
from pika.credentials import PlainCredentials
from pika.spec import Basic, BasicProperties

import logging

from pyboot2.util import DateTimeUtil


class RabbitMQMessage:
    def __init__(self, method_frame, header_frame, body):
        self.method_frame = method_frame  # type: Basic.GetOk
        self.header_frame = header_frame  # type: BasicProperties
        self.body = body  # type: bytes

    def get_delivery_tag(self):
        if self.method_frame: return self.method_frame.delivery_tag

    def get_routing_key(self):
        if self.method_frame: return self.method_frame.routing_key


class RabbitMQClient:
    def __init__(self, host, port=5672, username=None, password=None, virtual_host="/", enabled=True):
        self.__host = host
        self.__port = port
        self.__username = username
        self.__password = password
        self.__virtual_host = virtual_host
        self.__enabled = enabled

        self.connection = None  # type: Connection
        self.channel = None  # type: Channel

    def connect(self):
        if not self.__enabled:
            logging.info("RabbitMQClient is disabled, not connecting to queue")
            return

        credentials = PlainCredentials(self.__username, self.__password)
        self.connection = BlockingConnection(ConnectionParameters(
            host=self.__host, port=self.__port, virtual_host=self.__virtual_host, credentials=credentials))
        self.channel = self.connection.channel()

    def send(self, exchange, routing_key, body, properties=None, mandatory=None, immediate=False):
        if not self.__enabled:
            logging.info("RabbitMQClient is disabled, not sending message to queue")
            return

        return self.channel.basic_publish(
            exchange=exchange, routing_key=routing_key, body=body, properties=properties, mandatory=mandatory,
            immediate=immediate)

    def ack(self, delivery_tag=0, multiple=False):
        if not self.__enabled:
            logging.info("RabbitMQClient is disabled, not acknowledging to queue")
            return

        return self.channel.basic_ack(delivery_tag=delivery_tag, multiple=multiple)

    def reject(self, delivery_tag, requeue=True):
        if not self.__enabled:
            logging.info("RabbitMQClient is disabled, not acknowledging to queue")
            return

        return self.channel.basic_reject(delivery_tag=delivery_tag, requeue=requeue)

    def nack(self, delivery_tag=None, multiple=False, requeue=True):
        if not self.__enabled:
            logging.info("RabbitMQClient is disabled, not acknowledging to queue")
            return

        return self.channel.basic_nack(delivery_tag=delivery_tag, multiple=multiple, requeue=requeue)

    def get(self, queue, no_ack=False):
        if not self.__enabled:
            logging.info("RabbitMQClient is disabled, not getting message from queue")
            return

        method_frame, header_frame, body = self.channel.basic_get(queue=queue, no_ack=no_ack)
        if method_frame: return RabbitMQMessage(method_frame, header_frame, body)

    def disconnect(self):
        if not self.__enabled:
            logging.info("RabbitMQClient is disabled, not disconnecting from queue")
            return

        if not self.connection.is_open:
            self.connection.close()

    def is_connected(self):
        if not self.__enabled:
            logging.info("RabbitMQClient is disabled, returning True for is_connected()")
            return True

        return self.connection.is_open and self.channel.is_open


class RabbitMQConf:
    def __init__(self, host=None, port=5672, username=None, password=None, virtual_host="/", enabled=True):
        self.host = host
        self.port = port
        self.username = username
        self.password = password
        self.virtual_host = virtual_host
        self.enabled = enabled

    @contextmanager
    def get_client(self):
        start_time = datetime.datetime.now()
        client = RabbitMQClient(host=self.host, port=self.port, username=self.username, password=self.password,
                                virtual_host=self.virtual_host, enabled=self.enabled)
        client.connect()
        conn_time = datetime.datetime.now()
        logging.debug("RabbitMQ connection time: %s milliseconds" % DateTimeUtil.diff(start_time, conn_time))

        try:
            yield client  # type: RabbitMQClient
        finally:
            client.disconnect()
            logging.debug(
                "RabbitMQ transaction time: %s milliseconds" % DateTimeUtil.diff(conn_time, datetime.datetime.now()))
