import logging

import yaml


class Conf(object):
    def __init__(self, root_folder: str = None, app_conf_file: str = None):
        self.root_folder = root_folder
        self.app_conf_file = app_conf_file

        self.app_conf = None

    def init(self):
        stream = open(self.app_conf_file, "r")
        self.app_conf = yaml.load(stream)
        stream.close()
        logging.debug("Config initialized")
        return self

    def get(self, key: str, default=None):
        if not self.app_conf or key not in self.app_conf: return default
        return self.app_conf[key]

    def set(self, key: str, value: str):
        self.app_conf[key] = value

    def get_root_folder(self):
        return self.root_folder

    def set_root_folder(self, root_folder: str = None):
        self.root_folder = root_folder

    def get_app_conf_file(self) -> str:
        return self.app_conf_file

    def set_app_conf_file(self, app_conf_file: str = None):
        self.app_conf_file = app_conf_file
