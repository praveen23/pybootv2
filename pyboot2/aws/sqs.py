# class AmazonSQSClient(object):
#     def __init__(self, access_key_id, access_key_secret, region_name):
#         self.__connection = sqs.connect_to_region(
#             aws_access_key_id=access_key_id, aws_secret_access_key=access_key_secret, region_name=region_name)
#
#     def get_queue(self, name):
#         return self.__connection.get_queue(name)
#
#     def get_message(self, queue):
#         messages = queue.get_messages(1)
#         if len(messages) > 0:
#             return messages[0]
#
#     def get_multiple(self, queue, count):
#         return queue.get_messages(count)
#
#     def send(self, queue, message):
#         queue.write(message)
#
#     def delete(self, queue, message):
#         queue.delete_message(message)
#
#     def get_count(self, queue):
#         return queue.count()
#
#     def clear(self, queue):
#         queue.clear()
