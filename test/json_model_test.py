import sys, os

sys.path.insert(0, os.path.abspath("../"))

import datetime
from pyboot2.model import Model
from pyboot2.json import dump_json, load_json
from unittest import TestCase


class Employee(Model):
    _structure = {
        "id": int,
        "name": str,
        "start_date": datetime.date
    }

    def __init__(self, id=None, name=None, start_date=None):
        self.id = id
        self.name = name
        self.start_date = start_date


class Company(Model):
    _structure = {
        "id": int,
        "name": str,
        "start_date": datetime.date,
        "manager_id": int,
        "manager": "json_model_test.Employee",
        "all_employees": [Employee],
        "ceo_details": {
            "ceo": Employee,
            "start_date": datetime.date,
            "key1": {
                "key2": Employee
            },
            "key3": [Employee]
        }
    }


class ModelTest(TestCase):
    def test_to_dict_deep(self):
        company = Company()
        company.id = 10
        company.name = "Livspace"
        company.start_date = datetime.date.today()

        company.manager_id = 1

        rajeev = Employee(1, "Rajeev Sharma", "2013-04-29")
        vidur = Employee(2, "Vidur Jain", "2015-01-01")

        company.manager = rajeev
        company.all_employees = [rajeev, vidur]

        company.ceo_details = {
            "ceo": Employee(3, "Anuj Srivastava", "2014-01-01"),
            "start_date": datetime.date.today(),
            "key1": {
                "key2": Employee(4, "Anuj Srivastava", "2014-01-01")
            },
            "key3": [Employee(4, "Anuj Srivastava", "2014-01-01")]
        }

        print(dump_json(company.to_dict_deep()))

    def test_from_dict_deep(self):
        obj_dict = load_json(
            '{"id": 10, "name": "Livspace", "start_date": "2017-07-20", "manager_id": 1, "manager": {"id": 1, "name": "Rajeev Sharma", "start_date": "2013-04-29"}, "all_employees": [{"id": 1, "name": "Rajeev Sharma", "start_date": "2013-04-29"}, {"id": 2, "name": "Vidur Jain", "start_date": "2015-01-01"}], "ceo_details": {"ceo": {"id": 3, "name": "Anuj Srivastava", "start_date": "2014-01-01"}, "start_date": "2017-07-20", "key1": {"key2": {"id": 4, "name": "Anuj Srivastava", "start_date": "2014-01-01"}}, "key3": [{"id": 4, "name": "Anuj Srivastava", "start_date": "2014-01-01"}]}}')

        company = Company()
        company.from_dict_deep(obj_dict)

        print(company)
