import logging
import os
import sys

sys.path.insert(0, os.path.abspath("../"))

from unittest import TestCase

from pyboot2.util import NumberUtil


class NumberUtilTest(TestCase):
    def test_decimal_to_base(self):
        converted = NumberUtil.decimal_to_base(0, 36)
        self.assertEqual("0", converted)

        converted = NumberUtil.decimal_to_base(7, 36)
        self.assertEqual("7", converted)

        converted = NumberUtil.decimal_to_base(9, 36)
        self.assertEqual("9", converted)

        converted = NumberUtil.decimal_to_base(10, 36)
        self.assertEqual("a", converted)

        converted = NumberUtil.decimal_to_base(18, 36)
        self.assertEqual("i", converted)

        converted = NumberUtil.decimal_to_base(35, 36)
        self.assertEqual("z", converted)

        converted = NumberUtil.decimal_to_base(36, 36)
        self.assertEqual("10", converted)

        converted = NumberUtil.decimal_to_base(36, 36)
        self.assertEqual("10", converted)

        converted = NumberUtil.decimal_to_base(100148, 36)
        print(f"100148 => {converted.rjust(5, '0')}")
        # self.assertEqual("zzzzz", converted)

        converted = NumberUtil.decimal_to_base(60466175, 36)
        print(f"60466175 => {converted}")
        self.assertEqual("zzzzz", converted)

        for i in range(1, 100):
            converted = NumberUtil.decimal_to_base(i, 36)
            print(f"Conversion: {i} => {converted}")
            self.assertEqual(NumberUtil.base_to_decimal(converted, 36), i)

    def test_base_to_decimal(self):
        converted = NumberUtil.base_to_decimal("0", 36)
        self.assertEqual(0, converted)

        converted = NumberUtil.base_to_decimal("7", 36)
        self.assertEqual(7, converted)

        converted = NumberUtil.base_to_decimal("9", 36)
        self.assertEqual(9, converted)

        converted = NumberUtil.base_to_decimal("a", 36)
        self.assertEqual(10, converted)

        converted = NumberUtil.base_to_decimal("i", 36)
        self.assertEqual(18, converted)

        converted = NumberUtil.base_to_decimal("z", 36)
        self.assertEqual(35, converted)

        converted = NumberUtil.base_to_decimal("zzzzz", 36)
        print(f"zzzzz => {converted}")
        self.assertEqual(60466175, converted)

        # for i in range(1, 100):
        #     converted = NumberUtil.base_to_decimal(str(i), 36)
        #     print(f"Conversion: {i} => {converted}")
