import logging
from unittest import TestCase

from pyboot2.json import dump_json
from pyboot2.queue import RabbitMQConf
from pyboot2.queue import RabbitMQMessage


class QueueClientTest(TestCase):
    def setUp(self):
        self.username = "dev"
        self.password = "dev@123"
        logging.root.setLevel("DEBUG")

    def test_send(self):
        with RabbitMQConf(host="pankhi.com", username=self.username, password=self.password).get_client() as client:
            client.send("procure.direct.local", "REQUIREMENT_SENT_TO_SELLER", dump_json({"key": "value"}))
        logging.debug("Message sent")

    def test_get(self):
        with RabbitMQConf(host="pankhi.com", username=self.username, password=self.password).get_client() as client:
            message = client.get("seller.incoming_requirement_notification.local")  # type: RabbitMQMessage
            if message:
                logging.debug("Message body: %s" % message.body)
                client.ack(message.get_delivery_tag())
            else:
                logging.debug("No message")
